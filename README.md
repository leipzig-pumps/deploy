# deploy


## FugaCloud
 * Register / Login into [FugaCloud](https://fuga.cloud/)

### Key pair
 * left sidebar: Access -> Key pairs -> Create new key pair with name "FugaAccess"
 * download private key and store at under FugaAccess.pem in ~/.ssh, chmod 400 FugaAccess.pem

### Security Groups
#### Kubernetes Access
 * left sidebar: Networking -> Security groups -> Create security group
   * name: Kubernetes Access
   * Add rule: 
     * Custom TCP rule
     * direction: ingress
     * open port: 6443
     * description: Kubernetes control panel

### Create instances
#### Create instance for Kubernetes master
 * left sidebar: Compute -> Instances -> Create Instance
   * location: AMS2-Ra
   * boot source: Ubuntu 20.04 LTS
   * boot disk: Volume, 10 GB, tier-1, delete storage at instance delete: on
   * flavor: standard -> s3.small (2.05 GB, 1 vCPU) (Kubernates master actually requires 2GB RAM and 2 vCPU, but for testing this is just ok)
   * network: public
   * Key pair: FugaAccess
   * Security groups: SSH, default, Kubernetes Access
   * name: kubernetes-master
   * Advanced settings
     * disk partition: automatic
 * Estimated costs
   * s3.small | 2.05 GB Memory | 1 vCPU | 25 SSD Disk: €15.00/mo €0.02232/h
   * Volume store tier-1 boot disk (SSD): 10 GiB: €1.00/mo €0.00149/hr
   * Public IP: €1.00/mo €0.00149/hr
 * Launch instance

#### Create instance for Kubernetes worker
 * Compute -> Instances -> Create Instance
   * location: AMS2-Ra
   * boot source: Ubuntu 20.04 LTS
   * boot disk: Volume, 10 GB, tier-1, delete storage at instance delete: on
   * flavor: entry level -> t2.tiny (922 MB, 1 vCPU) (Kubernates workers actually requires 1GB RAM and 1 vCPU, but for testing this is just ok)
   * network: public
   * Key pair: FugaAccess
   * Security groups: SSH, default
   * name: kubernetes-worker-0
   * Advanced settings
     * disk partition: automatic

## SSH config
 * `nano ~/.ssh/config` with content
```
Host fuga-master
    HostName 81.24.11.109
    User ubuntu
    IdentityFile FugaAccess.pem

Host fuga-worker-0
    HostName 81.24.10.150
    User ubuntu
    IdentityFile FugaAccess.pem
```

## Ansible
 * see also this [manual](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-20-04) and the [cheat sheet](https://www.digitalocean.com/community/cheatsheets/how-to-use-ansible-cheat-sheet-guide)
 * Install via `sudo apt install ansible`
 * `sudo nano /etc/ansible/hosts` with content (look IP adresses of you just deployed instances under Compute -> Instances in FugaCloud)
```
[servers]
master ansible_host=81.24.10.150
worker-0 ansible_host=81.24.11.109

[all:vars]
ansible_python_interpreter=/usr/bin/python3
```
 * the instances should be both reachable via: `ansible all -m ping -u ubuntu --private-key=~/.ssh/FugaAccess.pem`
 * `cd ansible` and `ansible-playbook docker.yaml -u ubuntu --private-key=~/.ssh/FugaAccess.pem` to install Docker on master and worker node
 * `ansible-playbook kubernetes.yaml -u ubuntu --private-key=~/.ssh/FugaAccess.pem` to install Kubernetes on master and worker node

## Kubernetes
 * `kubectl get pods --all-namespaces` should now also on local machine (because the kube config has been copied from master to your local machine)
 * `kubectl get cs`

## GitLab
### Kubernetes Agent
 * create file .gitlab/agents/fugacloud/config.yaml in clusters project with the following content, to be able to share the kubernetes agent in all projects in GitLab group `leipzig-pumps`
```
ci_access:
  groups:
    - id: leipzig-pumps
``` 
 * GitLab left side panel: Infrastructure -> Kubernetes clusters -> Actions -> Connect with an agent -> select agent from drop down -> copy command (and store token separately)
 * execute command on Kubernetes master
 * agent entry in GitLab should turn green indicating successful connection
### Deploy Token
 * in GitLab go to group, e.g. https://gitlab.com/leipzig-pumps
 * GitLab left side panel: Repositories -> Expand Deploy Tokens
   * name: leipzig-pumps-deploy-token
   * username: leipzig-pumps-deploy-token-user
   * mark all read scopes
   * copy token value in store it your personal password store
 * GitLab left side panel of in each of your deployable projects: Settings -> CI/CD -> Expand Variables
   * CI\_DEPLOY\_USER = leipzig-pumps-deploy-token-user
   * CI\_DEPLOY\_PASSWORD = <deploy token>
### Pipeline
 * either execute Pipeline directly or let it execute automatically for every git push, it will trigger the deployment to Kubernetes
 * GitLab left side panel: Packages & Registry -> Container Registry will contain the docker image, that will be deployed
