#!/bin/sh
rm -f ./deployment/deployment.yaml
awk '(NR>1 && FNR==1){printf ("\n---\n")};1' \
./namespace.yaml \
./postgis/config.yaml \
./postgis/deployment.yaml \
./postgis/service.yaml \
./server/config.yaml \
./server/deployment.yaml \
./server/service.yaml \
./keycloak/deployment.yaml \
./keycloak/service.yaml \
./web/deployment.yaml \
./web/service.yaml \
./ingress/ingress.yaml \
>> ./deployment/deployment.yaml